from contextlib import asynccontextmanager

from psycopg import AsyncConnection
from psycopg.errors import DuplicateDatabase, DuplicateObject

from daoprueba.migrations.errors import NotFound  # type: ignore


async def create_database_if_not_exist(conn: AsyncConnection, database: str, user: str, password: str):
    print(f"- Creating database '{database}'...")

    async with with_auto_commit(conn):
        try:
            async with conn.cursor() as cur:
                await cur.execute(f"CREATE USER {user} WITH PASSWORD '{password}'")
                await cur.execute(f"CREATE DATABASE {database} WITH OWNER {user}")

        except (DuplicateDatabase, DuplicateObject):
            print(f"- ...not created because the database '{database}' already exists...")
            pass


async def create_table_if_not_exists(conn: AsyncConnection, table: str):
    print("- Creating versions tables if it doesn't exist...")

    async with conn.cursor() as cur:
        await cur.execute(f"CREATE TABLE IF NOT EXISTS {table} (version SERIAL PRIMARY KEY)")


async def select_current_version(conn: AsyncConnection) -> int:
    async with conn.cursor() as cur:
        await cur.execute("SELECT version FROM versions FOR UPDATE")
        record = await cur.fetchone()

        if not record:
            version = 0
            await cur.execute(f"INSERT INTO VERSIONS (version) VALUES ({version})")
        else:
            version = record[0]

        return version


async def increase_version(conn: AsyncConnection) -> int:
    async with conn.cursor() as cur:
        await cur.execute("UPDATE versions SET version = version + 1")

        version = -1

        await cur.execute("SELECT version FROM versions")
        record = await cur.fetchone()
        if record:
            version = record[0]
        else:
            raise NotFound

        return version


async def count_table(conn: AsyncConnection, table: str) -> int:
    async with conn.cursor() as cur:
        await cur.execute(f"SELECT count(*) FROM {table}")
        record = await cur.fetchone()

        if not record:
            raise Exception(f"no reord for count on table '{table}'")
        else:
            count = record[0]

        return count


@asynccontextmanager
async def with_auto_commit(conn: AsyncConnection):
    try:
        await conn.set_autocommit(True)
        yield
    finally:
        await conn.set_autocommit(False)
