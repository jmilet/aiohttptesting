from psycopg import AsyncConnection


async def migration_0000001(conn: AsyncConnection):
    print("- Applying migration migration_0000001...")

    async with conn.cursor() as cur:
        await cur.execute("CREATE TABLE IF NOT EXISTS one (id SERIAL PRIMARY KEY)")


async def migration_0000002(conn: AsyncConnection):
    print("- Applying migration migration_0000002...")

    async with conn.cursor() as cur:
        await cur.execute("CREATE TABLE IF NOT EXISTS two (id SERIAL PRIMARY KEY)")


async def migration_0000003(conn: AsyncConnection):
    print("- Applying migration migration_0000003...")

    async with conn.cursor() as cur:
        await cur.execute("CREATE TABLE IF NOT EXISTS three (id SERIAL PRIMARY KEY)")


async def migration_0000004(conn: AsyncConnection):
    print("- Applying migration migration_0000004...")

    async with conn.cursor() as cur:
        await cur.execute("CREATE TABLE IF NOT EXISTS four (id SERIAL PRIMARY KEY)")


async def migration_0000005(conn: AsyncConnection):
    print("- Applying migration migration_0000005...")

    async with conn.cursor() as cur:
        await cur.execute("CREATE TABLE IF NOT EXISTS five (id SERIAL PRIMARY KEY)")
