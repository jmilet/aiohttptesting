from pydantic import BaseSettings  # type: ignore


class Settings(BaseSettings):
    postgres_service_host: str
    postgres_service_port: int
    dbname: str
    user: str
    password: str
    redis_service_host: str
    redis_service_port: int
