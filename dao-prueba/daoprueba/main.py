import asyncio
import sys
from contextlib import asynccontextmanager

import redis.asyncio as redis  # type:ignore
from psycopg import AsyncConnection

from daoprueba.migrations import steps
from daoprueba.migrations.dao import (
    create_database_if_not_exist,
    create_table_if_not_exists,
    increase_version,
    select_current_version,
)
from daoprueba.settings import Settings  # type:ignore

INFINITE = 999999999


async def connection_from_settings(settings: Settings) -> AsyncConnection:
    return await AsyncConnection.connect(
        f"host='{settings.postgres_service_host}' port={settings.postgres_service_port} "
        f"user='{settings.user}' password='{settings.password}' dbname='{settings.dbname}'"
    )


async def create_database(db_to_create_settings):
    # Create databaase.
    default_db_settings = Settings(user="postgres", password="mysecretpassword", dbname="postgres")
    async with await connection_from_settings(default_db_settings) as conn:
        await create_database_if_not_exist(
            conn, db_to_create_settings.dbname, db_to_create_settings.user, db_to_create_settings.password
        )


async def migrate(settings: Settings, up_to: int = INFINITE):
    # Create databaase.
    await create_database(settings)

    # Migration.
    async with await connection_from_settings(settings) as conn:
        await create_table_if_not_exists(conn, "versions")

        version = await select_current_version(conn)

        print(f"- Last version is {version}...")

        while version < up_to:
            try:
                migration = getattr(steps, "migration_%07d" % (version + 1))

                print(f"- Starting migration application for {version + 1}...")
                await migration(conn)

                version = await increase_version(conn)
            except AttributeError:
                break

        version = await select_current_version(conn)

        print(f"- All pending migrations applied... The version is: {version}")


@asynccontextmanager
async def redis_lock(r: redis.Connection, key: str):
    try:
        if not await r.setnx(key, 1):
            print("- Lock detected")
            sys.exit(0)

        yield

    finally:
        await r.delete(key)


async def main(up_to: int = INFINITE):
    settings = Settings()

    r = redis.Redis(host=settings.redis_service_host, port=settings.redis_service_port, db=0)

    async with redis_lock(r, "jmi.service.migration"):
        await migrate(settings, up_to)


if __name__ == "__main__":
    asyncio.run(main())
