from unittest.mock import patch

import pytest

from daoprueba.main import connection_from_settings, main
from daoprueba.migrations.dao import count_table, select_current_version
from daoprueba.settings import Settings

pytestmark = pytest.mark.asyncio


settings = {
    "POSTGRES_SERVICE_HOST": "localhost",
    "POSTGRES_SERVICE_PORT": "5432",
    "REDIS_SERVICE_HOST": "localhost",
    "REDIS_SERVICE_PORT": "6379",
    "DBNAME": "pruebas",
    "USER": "theuser",
    "PASSWORD": "thepassword",
}


@pytest.mark.parametrize("up_to, table", [(1, "one"), (2, "two"), (3, "three"), (4, "four"), (5, "five")])
async def test_migration_0000001(up_to: int, table: str):
    with patch.dict("os.environ", settings):
        # Run the migration up to the given version.
        await main(up_to=up_to)

        # Create a new connection to the database.
        conn = await connection_from_settings(Settings())

        # The new table must exist.
        assert 0 == await count_table(conn, table=table)

        # The version must be the given one.
        assert up_to == await select_current_version(conn)
