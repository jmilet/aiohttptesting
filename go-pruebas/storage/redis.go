package storage

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

// Redis is a redis based key/value store.
type Redis struct {
	rdb *redis.Client
}

// NewRedis creates a Redis object.
func NewRedis(address, password string, db int) *Redis {
	rdb := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       db,
	})

	return &Redis{rdb: rdb}
}

// Put puts a value under the key in the datastore.
func (r *Redis) Put(key, value string) error {
	err := r.rdb.Set(context.Background(), key, value, 60*time.Second).Err()
	if err != nil {
		return fmt.Errorf("error in r.rdb.Set [%v]", err.Error())
	}

	return nil
}

// Get gets the value under the key in the datastore.
func (r *Redis) Get(key string) (string, error) {
	value, err := r.rdb.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return "", ErrNotFound
	} else if err != nil {
		return "", fmt.Errorf("error in r.rdb.Get [%v]", err.Error())
	}

	return value, nil
}
