package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMemory(t *testing.T) {
	memory := NewMemory()

	err := memory.Put("uno", "value uno")
	assert.NoError(t, err)

	value, err := memory.Get("uno")
	assert.NoError(t, err)
	assert.Equal(t, value, "value uno")

	_, err = memory.Get("dos")
	assert.Error(t, ErrNotFound)
}
