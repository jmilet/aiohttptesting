package storage

// Memory is a memory key val data store.
type Memory struct {
	data map[string]string
}

var _ KeyVal = &Memory{}

// NewMemory creates a Memory object.
func NewMemory() *Memory {
	return &Memory{data: make(map[string]string)}
}

// Put puts a value under the key in the datastore.
func (m *Memory) Put(key, value string) error {
	m.data[key] = value
	return nil
}

// Get gets the value under the key in the datastore.
func (m *Memory) Get(key string) (string, error) {
	value, ok := m.data[key]
	if !ok {
		return "", ErrNotFound
	}
	return value, nil
}
