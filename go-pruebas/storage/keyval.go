package storage

import "errors"

// ErrNotFound is not found error.
var ErrNotFound = errors.New("not found")

// KeyVal is a key value store.
type KeyVal interface {
	Put(key string, value string) error
	Get(key string) (string, error)
}
