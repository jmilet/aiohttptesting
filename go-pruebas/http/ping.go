package http

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// PingResult is a ping result.
type PingResult struct {
	Message string
}

// Ping is a ping.
func Ping(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "pong",
	})
}
