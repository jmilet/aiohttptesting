package http

import (
	"fmt"
	"io"

	"github.com/gin-gonic/gin"
	"gopruebas.com/storage"
)

// Set is a set.
func Set(db storage.KeyVal) gin.HandlerFunc {
	return func(c *gin.Context) {
		body, err := io.ReadAll(c.Request.Body)
		if err != nil {
			c.String(500, fmt.Sprintf("error in p.db.Set [%s]", err.Error()))
			return
		}

		err = db.Put(c.Param("id"), string(body))
		if err != nil {
			c.String(500, fmt.Sprintf("error in p.db.Set [%s]", err.Error()))
			return
		}

		c.Status(200)
	}
}

// Get is a get.
func Get(db storage.KeyVal) gin.HandlerFunc {
	return func(c *gin.Context) {
		value, err := db.Get(c.Param("id"))
		if err != nil {
			if err == storage.ErrNotFound {
				c.Status(404)
				return
			}

			c.String(500, fmt.Sprintf("error in p.db.Get [%s]", err.Error()))
			return
		}

		c.String(200, value)
	}
}
