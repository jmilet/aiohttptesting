package http

import (
	"github.com/gin-gonic/gin"
	"gopruebas.com/storage"
)

// SetupRouter sets the router up.
func SetupRouter(db storage.KeyVal) *gin.Engine {
	r := gin.Default()

	r.GET("/ping", Ping)
	r.POST("/store/:id", Set(db))
	r.GET("/store/:id", Get(db))

	return r
}
