package http

import (
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopruebas.com/mocks"
	"gopruebas.com/storage"
)

func TestStoreHappyPath(t *testing.T) {
	db := storage.NewMemory()

	router := SetupRouter(db)

	// Put.
	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPost, "/store/pepe", strings.NewReader("hola que tal"))
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 200, w.Code)

	// Get ok.
	w = httptest.NewRecorder()
	req, err = http.NewRequest(http.MethodGet, "/store/pepe", nil)
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 200, w.Code)

	// Check body.
	body, err := io.ReadAll(w.Body)
	assert.NoError(t, err)
	assert.Equal(t, string(body), "hola que tal")

	// Get not found.
	w = httptest.NewRecorder()
	req, err = http.NewRequest(http.MethodGet, "/store/pepe_not_found", nil)
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 404, w.Code)
}

type brokenReader struct{}

func (brokenReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("manual error")
}

func TestStorePutFailsOnReadBody(t *testing.T) {
	db := storage.NewMemory()

	router := SetupRouter(db)

	// Put.
	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPost, "/store/pepe", brokenReader{})
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 500, w.Code)

	// Check body.
	body, err := io.ReadAll(w.Body)
	assert.NoError(t, err)
	assert.Equal(t, string(body), "error in p.db.Set [manual error]")
}

func TestStorePutFailsOnGet(t *testing.T) {
	db := &mocks.KeyVal{}
	db.On("Get", "pepe").Return("", errors.New("manual error"))

	router := SetupRouter(db)

	// Get.
	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodGet, "/store/pepe", nil)
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 500, w.Code)

	// Check body.
	body, err := io.ReadAll(w.Body)
	assert.NoError(t, err)
	assert.Equal(t, "error in p.db.Get [manual error]", string(body))
}

func TestStorePutFailsOnPut(t *testing.T) {
	key := "pepe"
	value := "hola que tal pepe"

	db := &mocks.KeyVal{}
	db.On("Put", key, value).Return(errors.New("manual error"))

	router := SetupRouter(db)

	// Get.
	w := httptest.NewRecorder()
	req, err := http.NewRequest(http.MethodPost, "/store/pepe", strings.NewReader(value))
	assert.NoError(t, err)
	router.ServeHTTP(w, req)
	assert.Equal(t, 500, w.Code)

	// Check body.
	body, err := io.ReadAll(w.Body)
	assert.NoError(t, err)
	assert.Equal(t, "error in p.db.Set [manual error]", string(body))
}
