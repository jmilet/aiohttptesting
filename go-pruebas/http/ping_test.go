package http

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPing(t *testing.T) {
	router := SetupRouter(nil)

	w := httptest.NewRecorder()

	req, err := http.NewRequest(http.MethodGet, "/ping", nil)
	assert.NoError(t, err)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var got PingResult
	expected := PingResult{Message: "pong"}

	err = json.Unmarshal(w.Body.Bytes(), &got)
	assert.NoError(t, err)

	assert.Equal(t, got, expected)
}
