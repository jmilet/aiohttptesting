package main

import (
	"fmt"
	"os"

	"gopruebas.com/http"
	"gopruebas.com/storage"
)

func main() {
	var db storage.KeyVal

	host := os.Getenv("REDIS_SERVICE_HOST")
	port := os.Getenv("REDIS_SERVICE_PORT")

	if host == "" || port == "" {
		fmt.Println("Using in memory db...")
		db = storage.NewMemory()
	} else {
		address := fmt.Sprintf("%s:%s", host, port)

		fmt.Println("Redis host detected: " + host)
		fmt.Println("Redis port detected: " + port)
		fmt.Println(fmt.Sprintf("Using Redis on address %s", address))

		db = storage.NewRedis(address, "", 0)
	}

	http.SetupRouter(db).Run(":8000")
}
