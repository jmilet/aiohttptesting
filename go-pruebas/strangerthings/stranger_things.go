package strangerthings

// MyMockedObject is an interface just to play.
type MyMockedObject interface {
	DoSomething(int) (bool, error)
}
