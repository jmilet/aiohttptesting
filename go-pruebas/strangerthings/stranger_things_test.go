package strangerthings

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopruebas.com/mocks"
)

func TestSomething(t *testing.T) {
	testObj := mocks.NewMyMockedObject(t)

	// Set expectations.
	call1 := testObj.On("DoSomething", 123).Return(true, nil)
	testObj.On("DoSomething", 124).Return(true, nil).NotBefore(call1)

	// Exec expectation 1.
	res, err := testObj.DoSomething(123)
	assert.NoError(t, err)
	assert.Equal(t, true, res)

	// Exec expectation 2.
	res, err = testObj.DoSomething(124)
	assert.NoError(t, err)
	assert.Equal(t, true, res)
}
