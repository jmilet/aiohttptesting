class Data:
    def __init__(self, data: dict):
        self._data = data

    def get(self, element):
        return self._data.get(element)
