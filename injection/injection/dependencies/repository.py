from injection.dependencies.data import Data


class UnknownError(Exception):
    pass


class Repository:
    def __init__(self, data: Data = None):
        self._data = data or self.generate_some_data()

    def generate_some_data(self):
        return Data({str(i): "%010d" % i for i in range(10000)})

    def get(self, element: str) -> str:
        return self._data.get(element)
