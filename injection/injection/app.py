from fastapi import FastAPI

import injection.routers.foa as foa
import injection.routers.foo as foo
from injection.dependencies import Repository, Settings, TimeManager


# The app might receive all the dependencies.
# Default values are production ready and could be overriden at testing time.
# Only 'settings' is mandatory.
def new_app(
    settings: Settings,
    *,
    time_manager: TimeManager = TimeManager(),
    repository: Repository = Repository(),
):
    app = FastAPI()

    # Different routers can have differrent dependencies.
    # Having them at the router level prevents us to have to pass them around.
    router_foo = foo.new_router(settings, time_manager, repository)
    router_foa = foa.new_router(settings)

    app.include_router(router_foo)
    app.include_router(router_foa)

    return app
