from fastapi import APIRouter

from injection.dependencies import Settings


def new_router(settings: Settings):
    foo = APIRouter(prefix="/foa")

    @foo.get("/say_hi")
    async def say_hi():
        return f"hi my base_url is {settings.base_url}"

    return foo
