from fastapi import APIRouter, HTTPException

from injection.dependencies import Repository, Settings, TimeManager
from injection.dependencies.repository import UnknownError as RepositoryUnknownError


def new_router(settings: Settings, time_manager: TimeManager, repository: Repository):
    foo = APIRouter(prefix="/foo")

    @foo.get("/healt_check")
    async def health_check():
        return "ok"

    @foo.get("/base_url")
    async def get_base_url():
        return settings.base_url

    @foo.get("/now")
    async def get_now():
        return time_manager.now()

    @foo.get("/get/{element}")
    async def get_element(element: str):
        try:
            data = repository.get(element)
            if not data:
                raise HTTPException(status_code=404)
            return data
        except RepositoryUnknownError:
            raise HTTPException(status_code=500)

    return foo
