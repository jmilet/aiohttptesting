import injection.app as application
from injection.dependencies import Settings  # type: ignore

# This line is the only contact with os.environment via pydantic.
app = application.new_app(Settings())
