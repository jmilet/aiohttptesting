from unittest.mock import Mock

import pytest
from httpx import AsyncClient

from injection.app import new_app
from injection.dependencies.data import Data
from injection.dependencies.repository import Repository
from injection.dependencies.repository import UnknownError as RepositoryUnknownError
from tests.routers.fixtures import client, settings  # type: ignore

pytestmark = pytest.mark.asyncio


pytest.mark.usefixtures("settings", "client")


async def test_get_happy_path(client):
    response = await client.get("/foo/get/10")
    assert response.status_code == 200
    assert response.json() == "0000000010"


async def test_get_not_found(client):
    response = await client.get("/foo/get/non_existing")
    assert response.status_code == 404


async def test_get_raises_exception(settings):
    # We use mocks as usual.
    repository = Mock()

    repository.get.side_effect = RepositoryUnknownError()

    app = new_app(settings, repository=repository)

    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.get("/foo/get/non_existing")
        assert response.status_code == 500


async def test_get_custom_data(settings):
    # We choose at what level we want to mock or we want to fake.
    data = Data({"hola": "qué tal"})
    repository = Repository(data)

    app = new_app(settings, repository=repository)

    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.get("/foo/get/hola")
        assert response.status_code == 200
        assert response.json() == "qué tal"

        response = await client.get("/foo/get/non_existing")
        assert response.status_code == 404
