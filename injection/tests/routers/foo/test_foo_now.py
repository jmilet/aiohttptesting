from unittest.mock import Mock

import pytest
from httpx import AsyncClient

from injection.app import new_app
from tests.routers.fixtures import client, settings  # type: ignore

pytestmark = pytest.mark.asyncio


pytest.mark.usefixtures("settings", "client")


async def test_get_mocked_now(settings):
    time_manager = Mock()
    time_manager.now.side_effect = [1000, 1001]

    app = new_app(settings, time_manager=time_manager)

    async with AsyncClient(app=app, base_url="http://test") as client:
        response = await client.get("/foo/now")
        assert response.status_code == 200
        assert response.json() == 1000

        response = await client.get("/foo/now")
        assert response.status_code == 200
        assert response.json() == 1001


async def test_get_real_now(client):
    response = await client.get("/foo/now")
    assert response.status_code == 200
    assert response.json() > 10000
