import pytest

from tests.routers.fixtures import client, settings  # type: ignore

pytestmark = pytest.mark.asyncio


pytest.mark.usefixtures("settings", "client")


async def test_health_check(client):
    response = await client.get("/foo/healt_check")
    assert response.status_code == 200
    assert response.json() == "ok"
