from datetime import datetime
from unittest.mock import Mock

import pytest

from tests.routers.fixtures import client, settings  # type: ignore

pytestmark = pytest.mark.asyncio


pytest.mark.usefixtures("settings", "client")


async def test_base_url(settings, client):
    response = await client.get("/foo/base_url")
    assert response.status_code == 200
    assert response.json() == settings.base_url
