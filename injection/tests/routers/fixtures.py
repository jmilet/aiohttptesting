import pytest
from httpx import AsyncClient

from injection.app import new_app
from injection.dependencies import Settings


@pytest.fixture
def settings():
    return Settings(base_url="http://localhost")


@pytest.fixture
async def client(settings):
    app = new_app(settings)
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client
