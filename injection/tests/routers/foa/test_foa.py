from unittest.mock import Mock

import pytest
from httpx import AsyncClient

from injection.app import new_app
from tests.routers.fixtures import client, settings  # type: ignore

pytestmark = pytest.mark.asyncio


pytest.mark.usefixtures("settings", "client")


async def test_say_hi(settings, client):
    response = await client.get("/foa/say_hi")
    assert response.status_code == 200
    assert response.json() == f"hi my base_url is {settings.base_url}"
