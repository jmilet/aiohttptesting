import io
from typing import IO

from avro.datafile import DataFileReader, DataFileWriter
from avro.io import DatumReader, DatumWriter
from avro.schema import parse


def awro_writer(schema_content: str, output: IO[bytes]) -> DataFileWriter:
    parsed_schema = parse(schema_content)
    return DataFileWriter(output, DatumWriter(), parsed_schema)


def awro_reader(raw_bytes: bytes) -> DataFileReader:
    return DataFileReader(io.BytesIO(raw_bytes), DatumReader())


def test_avro():
    with open("test/user.avsc", "rt") as f:
        schema_lines = "".join(f.readlines())

    # Writing.
    output = io.BytesIO()
    with awro_writer(schema_lines, output) as writer:
        writer.append({"name": "Alyssa", "favorite_number": 256})
        writer.append({"name": "Ben", "favorite_number": 7, "favorite_color": "red"})
        writer.flush()

        raw_bytes = output.getvalue()

    # Reading.
    with awro_reader(raw_bytes) as reader:
        assert list(reader) == [
            {"name": "Alyssa", "favorite_number": 256, "favorite_color": None},
            {"name": "Ben", "favorite_number": 7, "favorite_color": "red"},
        ]
