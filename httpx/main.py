import pytest
import atexit
import httpx

import os, glob
from pact import Consumer, Provider, Term, Verifier  # type:ignore
from pact.matchers import get_generated_values  # type:ignore


pact = Consumer("Consumer").has_pact_with(Provider("Provider"))
pact.start_service()
atexit.register(pact.stop_service)


@pytest.fixture(autouse=True)
def run_around_tests():
    yield
    for f in glob.glob("./*.log"):
        os.remove(f)
    for f in glob.glob("./*.json"):
        os.remove(f)
    for f in glob.glob("./log/*"):
        os.remove(f)
    os.removedirs("log")


@pytest.mark.asyncio
async def test_pact():
    expected = {
        "args": {"search": "example"},
        "headers": {
            "x-forwarded-proto": "https",
            "x-forwarded-port": "443",
            "host": "postman-echo.com",
            "x-amzn-trace-id": Term(
                "Root=.-.{8}-.{24}", "Root=1-61c5f6f4-2565218018a1845173acd0d7"
            ),
            "content-length": "0",
            "cookie": "",
        },
        "url": "https://postman-echo.com/get?search=example",
    }

    path = "/get"
    query = {"search": "example"}

    pact.given("something").upon_receiving("somthing else").with_request(
        method="GET", path=path, query=query
    ).will_respond_with(200, body=expected)

    # Verify against mock.
    with pact:
        async with httpx.AsyncClient() as client:
            r = await client.get(pact.uri + path, params=query)
            assert r.json() == get_generated_values(expected)

    # Verify against provider.
    verifier = Verifier(
        provider="Provider", provider_base_url="https://postman-echo.com"
    )

    errors, _logs = verifier.verify_pacts("./consumer-provider.json")
    assert not errors
