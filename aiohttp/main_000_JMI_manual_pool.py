import asyncio
import random
from typing import Tuple, Iterable, Set
from asyncio import Task


async def espera(n: int) -> Tuple[int, int]:
    if n == 3:
        raise Exception()

    seconds = random.randint(1, 20)
    print(n, f"---started---, sleeping for {seconds} seconds")
    await asyncio.sleep(seconds)
    print(n, "---finished---")

    return n, seconds


async def pool(work: Iterable, size: int):
    pending: Set[Task] = set()

    for w in work:
        if len(asyncio.all_tasks()) > size:  # all_tasks seems to be a task and it counts itself, so the > and not >=.
            done, pending = await asyncio.wait(pending, return_when=asyncio.FIRST_COMPLETED)
            yield await asyncio.gather(*done, return_exceptions=True)

        pending.add(asyncio.create_task(w))

    for r in await asyncio.gather(*pending, return_exceptions=True):
        yield r


async def main():
    work = [espera(i) for i in range(10)]

    async for res in pool(work, size=3):
        print(res)

asyncio.run(main())
