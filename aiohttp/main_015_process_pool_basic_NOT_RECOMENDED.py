from multiprocessing import Pool
import multiprocessing

# --------------------------------------------------------------------
# NOT RECOMMENDED: SEE THE FOLLOWING EXAMPLES
# --------------------------------------------------------------------


def say_hello(name: str) -> str:
    return f'Hi there, {name}'


if __name__ == '__main__':
    print(f'We have {multiprocessing.cpu_count()} CPUs')

    with Pool(processes=8) as pool:
        res1 = pool.apply_async(say_hello, args=('Juan',))
        res2 = pool.apply_async(say_hello, args=('Pepe',))

        print(res1.get())
        print(res2.get())
