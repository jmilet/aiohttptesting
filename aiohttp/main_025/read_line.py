import sys
from asyncio import StreamReader
from collections import deque
from typing import Deque
from main_025.ansi import move_back_one_char, clear_line  # type:ignore


async def read_line(stdin_reader: StreamReader) -> str:
    def erase_last_char():
        move_back_one_char()
        sys.stdout.write(' ')
        move_back_one_char()

    delete_char = b'\x7f'

    input_buffer: Deque = deque()

    while (input_char := await stdin_reader.read(1)) != b'\n':
        if input_char == delete_char:
            if len(input_buffer) > 0:
                input_buffer.pop()
                erase_last_char()
        else:
            input_buffer.append(input_char)
            sys.stdout.write(input_char.decode())

    clear_line()
    return b''.join(input_buffer).decode()
