# https://docs.aiohttp.org/en/stable/client_quickstart.html

import asyncio
from aiohttp import ClientSession, ClientTimeout


async def main():
    session_timeout = ClientTimeout(total=0)

    async with ClientSession(timeout=session_timeout) as session:
        async with session.get('https://stream.meetup.com/2/rsvps') as resp:
            async for line in resp.content:
                print(line)

asyncio.run(main())
