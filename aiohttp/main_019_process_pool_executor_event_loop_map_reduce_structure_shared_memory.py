from asyncio.tasks import wait_for
from concurrent.futures import ProcessPoolExecutor, process
from multiprocessing import Value
from functools import partial
import multiprocessing
import asyncio

progress: Value  # type: ignore


def init(progress_param):
    global progress
    progress = progress_param


def increment_progress():
    with progress.get_lock():
        progress.value += 1


async def report_progress(progress, num_tasks):
    # NOTE: In the book it's not passing the counter, but in my example it doesn't see it
    # from the coroutine.
    # It's working, but I'd like to know why in the book supposedly is.
    # Anyway, I don't like having sharing state, I hope we find a better way next
    # in the book.

    while progress.value < num_tasks:
        print(f'Finished {int(progress.value)}/{num_tasks}')
        await asyncio.sleep(3)

    print(f'Finished {int(progress.value)}/{num_tasks}')


def main_for_the_process(sleep_seconds: int) -> None:
    # This is the starting point of the process that runs into the pool
    # so we create a new event loop for it. This way we're running
    # different parallel processes with its own pid and event loop
    # each.

    async def _main():
        await asyncio.sleep(sleep_seconds)

        increment_progress()

        print(f'slept for {sleep_seconds}')
        return sleep_seconds

    return asyncio.run(_main())


async def main():
    print(f'We have {multiprocessing.cpu_count()} CPUs')

    loop = asyncio.get_event_loop()
    numbers = [1, 3, 5, 2, 4]
    calls = [partial(main_for_the_process, num) for num in numbers]

    progress = Value('d', 0)

    with ProcessPoolExecutor(initializer=init, initargs=(progress,)) as pool:

        coros = [loop.run_in_executor(pool, call) for call in calls]
        reporter = asyncio.create_task(report_progress(progress, len(coros)))

        # # --- Gather semantics ---
        # results = await asyncio.gather(*coros)
        # for result in results:
        #     print(result)

        # --- As completed semantics ---
        results = asyncio.as_completed(coros)
        for result in results:
            result = await result
            print(f'The process returned {result}')

        await reporter

if __name__ == '__main__':
    asyncio.run(main())
