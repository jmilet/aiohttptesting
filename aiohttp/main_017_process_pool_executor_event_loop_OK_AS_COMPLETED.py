from concurrent.futures import ProcessPoolExecutor, process
from functools import partial
import multiprocessing
import time
import asyncio


def count(count_to: int) -> int:
    start = time.time()

    counter = 0
    while counter < count_to:
        counter += 1

    end = time.time()

    print(f'Finishied counting from to {count_to} in {end - start}')
    return counter


async def main():
    print(f'We have {multiprocessing.cpu_count()} CPUs')

    loop = asyncio.get_event_loop()
    numbers = [100000000, 1, 3, 5, 22]
    calls = [partial(count, num) for num in numbers]

    with ProcessPoolExecutor() as pool:
        coros = [loop.run_in_executor(pool, call) for call in calls]

        # # --- Gather semantics ---
        # results = await asyncio.gather(*coros)
        # for result in results:
        #     print(result)

        # --- As completed semantics ---
        results = asyncio.as_completed(coros)
        for result in results:
            print(await result)

if __name__ == '__main__':
    asyncio.run(main())
