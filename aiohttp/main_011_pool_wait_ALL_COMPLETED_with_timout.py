import asyncio
import aiohttp
from aiohttp import ClientSession


async def fetch(session, url, sleep_seconds):
    await asyncio.sleep(sleep_seconds)
    async with session.get(url) as resp:
        return sleep_seconds, resp.status


async def main():
    async with aiohttp.ClientSession() as session:
        # Url and broken url.
        url = 'https://example.com'
        broken_url = 'pepe://example.com'

        # Run requests.
        pending = [
            asyncio.create_task(fetch(session, url, 2)),
            asyncio.create_task(fetch(session, broken_url, 4)),
            asyncio.create_task(fetch(session, url, 6))
        ]

        done, pending = await asyncio.wait(pending, return_when=asyncio.ALL_COMPLETED, timeout=3)

        print(f'Done task count: {len(done)}')
        print(f'Pending task count: {len(pending)}')

        for done_task in done:
            # Actually at this point we could choose between logically checking the exception/result
            # or directly await and handle the exception.
            # This way seems nicer.
            # result = await done_test

            if not done_task.exception():
                result = done_task.result()
                print(result)
            else:
                print(f'Got an exception: {done_task.exception()}')

        for pending_task in pending:
            print('cancelling task...')
            pending_task.cancel()

asyncio.run(main())
