import requests  # type: ignore
import asyncio

# This example runs each call in a Thread. This is useful when we have blocking legacy
# code and still we want to avoid blocking.
# Anyway, the recommended way of doing this is with non blocking libraries.
# According to the book the pure async example run 1000 calls under a second,
# while this example took more than a minute.


def get_status_code(url, num):
    response = requests.get(url)
    return num, response.status_code


async def main():
    url = 'http://example.com'

    tasks = [asyncio.to_thread(get_status_code, url, i) for i in range(1000)]

    for result in asyncio.as_completed(tasks):
        print(await result)


if __name__ == '__main__':
    asyncio.run(main())
