import asyncio


# More info about stream operators (the functional way)
#
#   https://aiostream.readthedocs.io/en/stable/operators.html

async def positive_integers(n):
    for i in range(n):
        await asyncio.sleep(1)
        yield i


async def main():
    async for i in positive_integers(10):
        print(i)

asyncio.run(main())
