import hashlib
import os
import time
import random


def random_password(length: int) -> bytes:
    ascii_lowercase = b'abcdefghijklmnopqrstuvwxyz'
    return b''.join(bytes(random.choice(ascii_lowercase)) for _ in range(length))


def hash(password: bytes) -> str:
    salt = os.urandom(16)
    return str(hashlib.scrypt(password=password, salt=salt, n=2, p=24, r=16))


def main():
    passwords = [random_password(10) for _ in range(10000)]
    start = time.time()
    for password in passwords:
        hash(password)
    end = time.time()
    print(end - start)


main()
