import hashlib
import os
import time
import random
import functools
import asyncio
from concurrent.futures.thread import ThreadPoolExecutor

# This solution makes some sense because the low level libraries are written in C, so they
# are releasing the GIL.


def random_password(length: int) -> bytes:
    ascii_lowercase = b'abcdefghijklmnopqrstuvwxyz'
    return b''.join(bytes(random.choice(ascii_lowercase)) for _ in range(length))


def hash(password: bytes) -> str:
    salt = os.urandom(16)
    return str(hashlib.scrypt(password=password, salt=salt, n=2, p=24, r=16))


async def main():
    passwords = [random_password(10) for _ in range(10000)]

    loop = asyncio.get_running_loop()

    start = time.time()

    with ThreadPoolExecutor() as pool:
        tasks = [loop.run_in_executor(pool, functools.partial(hash, password)) for password in passwords]

    await asyncio.gather(*tasks)

    end = time.time()
    print(end - start)


asyncio.run(main())
