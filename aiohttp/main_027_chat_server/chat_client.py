from main_025.asynchronous_standard_reader import create_stdin_reader  # type:ignore
from main_025.read_line import read_line  # type:ignore
from main_025.message_store import MessageStore  # type:ignore
from main_025.ansi import *  # type:ignore
import asyncio
import os
import logging
import tty
import sys
from asyncio import StreamReader, StreamWriter
from collections import deque


async def send_message(message: str, writer: StreamWriter):
    writer.write((message+'\n').encode())
    await writer.drain()


async def listen_for_messages(reader: StreamReader, message_store: MessageStore):
    while (message := await reader.readline()) != b'':
        await message_store.append(message.decode())
    await message_store.append('Server closed connection.')


async def read_and_send(stdin_reader: StreamReader, writer: StreamWriter):
    while True:
        message = await read_line(stdin_reader)
        await send_message(message, writer)


async def main():
    async def redraw_output(items: deque):
        save_cursor_position()  # type:ignore
        move_to_top_of_screen()  # type:ignore
        for item in items:
            delete_line()  # type:ignore
            sys.stdout.write(item)
        restore_cursor_position()  # type:ignore

    tty.setcbreak(0)
    os.system('clear')
    rows = move_to_bottom_of_screen()

    messages = MessageStore(redraw_output, rows-1)
    stdin_reader = await create_stdin_reader()
    sys.stdout.write('Enter user name: ')
    username = await read_line(stdin_reader)

    reader, writer = await asyncio.open_connection('0.0.0.0', 8000)

    writer.write(f'CONNECT {username}\n'.encode())
    await writer.drain()

    message_lister = asyncio.create_task(listen_for_messages(reader, messages))
    input_listener = asyncio.create_task(read_and_send(stdin_reader, writer))

    try:
        await asyncio.wait([message_lister, input_listener], return_when=asyncio.FIRST_COMPLETED)
    except Exception as ex:
        logging.exception(ex)
        writer.close()
        await writer.wait_closed()

asyncio.run(main())
