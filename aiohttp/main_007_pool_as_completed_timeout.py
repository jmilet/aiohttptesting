import asyncio
import aiohttp
from aiohttp import ClientSession


async def fetch(session, url, sleep_seconds):
    await asyncio.sleep(sleep_seconds)
    async with session.get(url) as resp:
        return sleep_seconds, resp.status


async def main():
    async with aiohttp.ClientSession() as session:
        # Set the tasks in reverse weight order.
        urls = ['https://example.com' for _ in range(3, 0, -1)]

        # Run requests.
        requests = [fetch(session, url, seconds + 1) for seconds, url in enumerate(urls)]

        # Process as completed, we see the normal weight order.
        for status_code in asyncio.as_completed(requests, timeout=2.5):
            try:
                print(await status_code)
            except asyncio.TimeoutError:
                print("timeout...")

asyncio.run(main())
