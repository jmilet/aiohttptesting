from concurrent.futures import ProcessPoolExecutor, process
import multiprocessing
import time

# --------------------------------------------------------------------
# NOT RECOMMENDED: SEE THE FOLLOWING EXAMPLES
# --------------------------------------------------------------------


def count(count_to: int) -> int:
    start = time.time()

    counter = 0
    while counter < count_to:
        counter += 1

    end = time.time()

    print(f'Finishied counting from to {count_to} in {end - start}')
    return counter


if __name__ == '__main__':
    print(f'We have {multiprocessing.cpu_count()} CPUs')

    with ProcessPoolExecutor() as pool:
        # The result delivery order is respected.
        # Blocks until all the results are done.

        numbers = [100000000, 1, 3, 5, 22]
        for result in pool.map(count, numbers):
            print(result)
