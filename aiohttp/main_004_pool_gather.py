import asyncio
import aiohttp
from aiohttp import ClientSession


async def fetch(session, url):
    async with session.get(url) as resp:
        return resp.status


async def main():
    async with aiohttp.ClientSession() as session:
        urls = ['https://example.com' for _ in range(1000)]
        requests = [fetch(session, url) for url in urls]
        status_codes = await asyncio.gather(*requests)
        print(status_codes)

asyncio.run(main())
