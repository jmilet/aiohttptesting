# https://docs.aiohttp.org/en/stable/client_quickstart.html

import asyncio
from aiohttp import ClientSession


async def call_example(session):
    async with session.get('http://www.example.com') as resp:
        print(await resp.text())


async def call_github(session):
    async with session.get('https://api.github.com/events') as resp:
        print(await resp.json())


async def main():
    async with ClientSession() as session:
        ex = asyncio.create_task(call_example(session))
        git = asyncio.create_task(call_github(session))
        await ex
        await git

asyncio.run(main())
