import functools
import asyncio
import multiprocessing
from concurrent.futures import ProcessPoolExecutor
from typing import Dict, Iterable, List, Any
from pytest import mark


def partition(data: Iterable, chunk_size: int):
    elements: List[Any] = []

    for element in data:
        if len(elements) < chunk_size:
            elements.append(element)
        else:
            yield elements
            elements = [element]

    yield elements


def test_partition():
    data = "esto es un bonito texto que sirve de datos a particionar".split(" ")
    expected = [
        ['esto', 'es', 'un', 'bonito'],
        ['texto', 'que', 'sirve', 'de'],
        ['datos', 'a', 'particionar']
    ]
    assert expected == list(partition(data, 4))

    assert [[]] == list(partition([], 4))

    assert [['a']] == list(partition(['a'], 4))


def map_frequency(text: str) -> Dict[str, int]:
    result: Dict[str, int] = {}

    words = text.replace(",", "").replace(".", "").split(" ")
    for w in words:
        result.setdefault(w, 0)
        result[w] += 1

    return result


def test_map_frequency():
    assert {'hola': 2, 'que': 2, 'tal': 2, 'bien': 1} == map_frequency("hola que tal, hola que tal bien")


def merge_dicts(first: Dict[str, int], second: Dict[str, int]) -> Dict[str, int]:
    res = dict(first)

    for k, v in second.items():
        res.setdefault(k, 0)
        res[k] += second[k]

    return res


def test_merge_dicts():
    first = {'a': 1}
    second = {'a': 1, 'b': 5}
    third = {'a': 1, 'b': 5, 'c': 20}

    assert {'a': 3, 'b': 10, 'c': 20} == functools.reduce(merge_dicts, [first, second, third])


def foo(data):
    return {
        str(data[0]): data[1]
    }


@mark.asyncio
async def test_map_reduce():
    print(f'We have {multiprocessing.cpu_count()} CPUs')

    loop = asyncio.get_event_loop()
    numbers = partition([(1, 100), (2, 1), (3, 3), (4,  5), (6,  22),
                         (1,   2), (2, 8), (3, 9), (4, 50), (6, 202)], 2)

    final_result = {}

    with ProcessPoolExecutor() as pool:
        for num_chunk in numbers:
            calls = [functools.partial(foo, num) for num in num_chunk]
            coros = [loop.run_in_executor(pool, call) for call in calls]

            # --- As completed semantics ---
            results = asyncio.as_completed(coros)
            for result in results:
                final_result = merge_dicts(final_result, await result)

    assert {'1': 102, '2': 9, '3': 12, '4': 55, '6': 224} == final_result
