import asyncio
import aiohttp
from aiohttp import ClientSession


async def fetch(session, url, sleep_seconds):
    await asyncio.sleep(sleep_seconds)
    async with session.get(url) as resp:
        return sleep_seconds, resp.status


async def main():
    async with aiohttp.ClientSession() as session:
        url = 'https://example.com'

        task_a = asyncio.create_task(fetch(session, url, 2))
        task_b = asyncio.create_task(fetch(session, url, 4))

        done, pending = await asyncio.wait([task_a, task_b], return_when=asyncio.FIRST_COMPLETED)

        for task in pending:
            # The reason to wrap is here. If we pass coroutines instead of tasks, the wait
            # function will create the wrapping task for us and then we lose the capacity
            # to compare directly compare the tasks objects.
            #
            # If we wrap in a task we'll be returned our very same task, so the comparison
            # is totally correct.
            if task is task_b:
                print('cancelling task b')
                task.cancel()

asyncio.run(main())
