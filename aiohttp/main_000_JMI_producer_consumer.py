import asyncio
import random


async def worker(name, logic, input, output=None, timeout=3):
    while True:
        try:
            value = await input.get()

            result = await asyncio.wait_for(logic(name, value), timeout)

            if output:
                await output.put(result)

        except asyncio.CancelledError:
            return
        except Exception as ex:
            await output.put((name, f'value {value}', ex))
        finally:
            input.task_done()


async def producer(name, value):
    seconds = random.randint(1, 4)

    job_desc = f'value {value} seconds {seconds}'

    # print(f'{name} starting for {job_desc}')

    if value == 3:
        raise Exception(f'Elem 3 exception for {job_desc}')

    await asyncio.sleep(seconds)

    return f'{job_desc} ok'


async def consumer(name, value):
    print(f'{name} received {value}')


async def main():
    load = 10
    queue_size = 2
    num_workers = 2

    input = asyncio.Queue(queue_size)
    output = asyncio.Queue(queue_size)

    tasks = \
        [asyncio.create_task(worker(f'worker1-{i}', producer, input, output, timeout=2)) for i in range(num_workers)] + \
        [asyncio.create_task(worker(f'worker2-{i}', consumer, output)) for i in range(num_workers)]

    print("--- queuing started ---")

    for i in range(load):
        await input.put(i)

    print("--- queuing ended ---")

    await input.join()
    await output.join()

    for task in tasks:
        task.cancel()

    await asyncio.gather(*tasks, return_exceptions=True)

asyncio.run(main())
