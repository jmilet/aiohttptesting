import asyncio
import aiohttp
from aiohttp import ClientSession


async def fetch(session, url):
    async with session.get(url) as resp:
        return resp.status


async def main():
    async with aiohttp.ClientSession() as session:
        urls = ['https://example.com' for _ in range(10)]

        # Break one url.
        urls[3] = 'abc'

        # Run requests.
        requests = [fetch(session, url) for url in urls]
        status_codes = await asyncio.gather(*requests, return_exceptions=True)

        # Filter out success and errors.
        success = [res for res in status_codes if not isinstance(res, Exception)]
        errors = [res for res in status_codes if isinstance(res, Exception)]
        print(success)
        print(errors)

asyncio.run(main())
