import asyncio


async def delay(seconds: int) -> None:
    print(f'Waiting for {seconds} seconds...')
    await asyncio.sleep(seconds)
    print(f'Waiting for {seconds} seconds finished...')
