# https://docs.aiohttp.org/en/stable/client_quickstart.html

import asyncio
from aiohttp import ClientSession, ClientTimeout


async def call_example(session):
    async with session.get('http://www.example.com') as resp:
        print(await resp.text())


async def call_github(session):
    client_timeout = ClientTimeout(total=1)

    async with session.get('https://api.github.com/events', timeout=client_timeout) as resp:
        print(await resp.json())


async def main():
    session_timeout = ClientTimeout(total=1, connect=0.7)

    async with ClientSession(timeout=session_timeout) as session:
        ex = asyncio.create_task(call_example(session))
        git = asyncio.create_task(call_github(session))
        await ex
        await git

asyncio.run(main())
