*************
Testing Notes
*************

::

    -s to show stdout.
    -k to run by pattern.
    -m run by marker. Notice the "pytestmark = pytest.mark.web" in the test file and the entry in "pyproject.toml".

        [tool.pytest.ini_options]
        markers = ["router"]

    pytest -s tests/test_main.py::test_put_foo
    pytest -s -k test_put
    pytest -s -m router