import asyncio

from pruebas.protobufs.greetings_pb2 import GreetingsResponse  # type:ignore
from pruebas.protobufs.greetings_grpc import GreetingsServiceBase  # type:ignore
from grpclib.server import Server
from grpclib.utils import graceful_exit


class GreetingsService(GreetingsServiceBase):
    def salute(self, request, context):
        print(f"request:{request}")
        return GreetingsResponse(message=f"guay!!!! - {request.message}")

    async def salute_stream(self, stream):
        i = 0
        while value := await stream.recv_message():
            print(f"request:{value}")
            await stream.send_message(GreetingsResponse(message=f"guay!!!! - {i} - {value.message}"))
            i += 1
            await asyncio.sleep(1)


async def main(*, host="127.0.0.1", port=50051):
    server = Server([GreetingsService()])
    # Note: graceful_exit isn't supported in Windows
    with graceful_exit([server]):
        await server.start(host, port)
        print(f"Serving on {host}:{port}")
        await server.wait_closed()


if __name__ == "__main__":
    asyncio.run(main())
