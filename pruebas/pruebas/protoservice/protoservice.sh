#!/bin/sh

export PYTHONPATH=/app/.venv/lib/python3.10/site-packages:/app

exec /app/.venv/bin/python -u /app/pruebas/protoservice/main.py
