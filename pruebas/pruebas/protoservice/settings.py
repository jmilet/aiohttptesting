from pydantic import BaseSettings


class Settings(BaseSettings):
    port: str
    max_workers: int
