import pydantic


class GreetingsRequest(pydantic.BaseModel):
    message: str


class GreetingsResponse(pydantic.BaseModel):
    message: str
