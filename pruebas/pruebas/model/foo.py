from pydantic import BaseModel


class Foo(BaseModel):
    the_id: int
    the_description: str
