from fastapi import FastAPI
from starlette.middleware.base import BaseHTTPMiddleware
from starlette_exporter import PrometheusMiddleware  # type: ignore
from starlette_exporter import handle_metrics

from pruebas.middleware.time import add_process_time_header
from pruebas.router.foo import foo
from pruebas.router.protobuf import protobuf
from pruebas.router.redis import redis

app = FastAPI()

# Middlewares.
app.add_middleware(PrometheusMiddleware)
app.add_middleware(BaseHTTPMiddleware, dispatch=add_process_time_header)

# Routes.
app.add_route("/metrics", handle_metrics)
app.include_router(foo)
app.include_router(redis)
app.include_router(protobuf)
