import asyncio
import uuid

import httpx
from pydantic import BaseSettings


class Settings(BaseSettings):
    pruebas_service_host: str = "localhost"
    pruebas_service_port: str = "8000"

    @property
    def url(self):
        return f"http://{self.pruebas_service_host}:{self.pruebas_service_port}/redis/publish"


async def main():
    settings = Settings()

    print(f"The settings are: {settings}")

    headers = {"Content-Type": "text/plain"}

    while True:
        value = {"field1": str(uuid.uuid4())}

        async with httpx.AsyncClient(timeout=30) as client:
            response = await client.post(settings.url, headers=headers, content=str(value))  # We don't want an actual json body, but a string.
            if response.status_code != 200:
                raise Exception(f"impossible to send message: {value} / status_code: {response.status_code}")

        await asyncio.sleep(2)

        print(f"sent '{value}'")


asyncio.run(main())
