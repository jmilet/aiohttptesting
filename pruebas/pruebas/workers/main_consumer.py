import asyncio

import aioredis

from pruebas.settings import Settings


async def get_value_from_queue(redis_db: aioredis.Redis, queue: str) -> str:
    return await redis_db.brpop(queue)


async def main_loop(redis_db: aioredis.Redis, queue: str) -> None:
    while True:
        value = await get_value_from_queue(redis_db, queue)
        print(f"received '{value}'")


async def main(settings: Settings):
    print(f"The settings are: {settings}")

    conn_str = f"redis://{settings.redis_service_host}:{settings.redis_service_port}"
    redis_db = aioredis.from_url(conn_str)

    await main_loop(redis_db=redis_db, queue=settings.redis_queue)


if __name__ == "__main__":
    settings = Settings()
    asyncio.run(main(settings))
