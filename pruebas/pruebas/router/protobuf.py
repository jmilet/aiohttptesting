import grpc
from fastapi import APIRouter

from pruebas.model.greeting import GreetingsRequest, GreetingsResponse
from pruebas.protobufs.greetings_pb2 import GreetingsRequest as GreetingsRequestGrpc
from pruebas.protobufs.greetings_pb2_grpc import GreetingsServiceStub
from pruebas.settings import Settings

settings = Settings()

protobuf = APIRouter(prefix="/protobuf")

channel = grpc.aio.insecure_channel(f"{settings.protobuf_service_host}:{settings.protobuf_service_port}")
grpc_client = GreetingsServiceStub(channel)


@protobuf.post("/greetings-service/salute", response_model=GreetingsResponse, response_model_exclude_none=True)
async def salute(request: GreetingsRequest) -> GreetingsResponse:
    message = GreetingsRequestGrpc(**request.dict())
    response = await grpc_client.salute(message)
    return GreetingsResponse(message=response.message)


@protobuf.post("/greetings-service/salute_streaming", response_model=list[GreetingsResponse], response_model_exclude_none=True)
async def salute_stream(request: GreetingsRequest) -> list[GreetingsResponse]:
    message = GreetingsRequestGrpc(**request.dict())

    message_iterator = (message for _ in range(10))

    output = []

    async for m in grpc_client.salute_stream(message_iterator):
        print(m)
        output.append(GreetingsResponse(message=m.message))

    return output
