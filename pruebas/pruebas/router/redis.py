from functools import lru_cache

import aioredis
from fastapi import APIRouter, Body, Response

from pruebas.settings import settings

redis = APIRouter(prefix="/redis")


@lru_cache(maxsize=1)
def redis_database():
    conn_str = f"redis://{settings.redis_service_host}:{settings.redis_service_port}"
    return aioredis.from_url(conn_str)


@redis.post("/publish")
async def redis_get(value: str = Body(None, min_length=1)):
    redis_db = redis_database()
    await redis_db.rpush(settings.redis_queue, value)
    return Response(status_code=200)
