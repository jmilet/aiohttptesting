import random
from dataclasses import dataclass
from functools import lru_cache
from typing import Optional

from fastapi import APIRouter, Depends, Response
from prometheus_client import Counter  # type: ignore
from pydantic import BaseModel

from pruebas.model.foo import Foo

foo = APIRouter(prefix="/foo")

c = Counter("visits", "number of visits", ["method", "endpoint"])


@lru_cache(maxsize=1)
def get_value():
    random.seed()
    return random.randint(1, 100)


@foo.get("")
async def root():
    c.labels(method="GET", endpoint="/").inc()
    return {"message": "Hello World"}


@foo.get("/health")
async def health():
    return Response(status_code=200)


@foo.put("")
async def put_foo(foo: Foo):
    return foo


@foo.get("/dependency")
async def get_value_from_dependency(value: int = Depends(get_value)):
    return value


@dataclass
class Params:
    q: str
    z: str


@foo.get("/dependency/params")
async def get_value_from_dependency_and_params(value: int = Depends(get_value), params: Params = Depends()):
    return f"{value}-{params.q}-{params.z}"


class ResponseModelExample(BaseModel):
    a: int
    b: str
    c: str
    d: Optional[str]


@foo.get("/response-model", response_model=ResponseModelExample, response_model_exclude_none=True)
async def response_model_example() -> dict:
    return {"a": 100, "b": "hola", "c": "pepe"}
