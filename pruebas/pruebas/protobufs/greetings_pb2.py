# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: greetings.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import builder as _builder
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0fgreetings.proto\"#\n\x10GreetingsRequest\x12\x0f\n\x07message\x18\x01 \x01(\t\"$\n\x11GreetingsResponse\x12\x0f\n\x07message\x18\x01 \x01(\t2\x7f\n\x10GreetingsService\x12/\n\x06salute\x12\x11.GreetingsRequest\x1a\x12.GreetingsResponse\x12:\n\rsalute_stream\x12\x11.GreetingsRequest\x1a\x12.GreetingsResponse(\x01\x30\x01\x62\x06proto3')

_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, globals())
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'greetings_pb2', globals())
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _GREETINGSREQUEST._serialized_start=19
  _GREETINGSREQUEST._serialized_end=54
  _GREETINGSRESPONSE._serialized_start=56
  _GREETINGSRESPONSE._serialized_end=92
  _GREETINGSSERVICE._serialized_start=94
  _GREETINGSSERVICE._serialized_end=221
# @@protoc_insertion_point(module_scope)
