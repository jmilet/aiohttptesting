from pydantic import BaseSettings


class Settings(BaseSettings):
    redis_service_host: str = "localhost"
    redis_service_port: str = "6379"
    redis_queue: str = "queue:fastapi"
    protobuf_service_host: str = "localhost"
    protobuf_service_port: str = "50051"


settings = Settings()
