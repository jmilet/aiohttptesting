import uuid


def test_uuid():
    value = uuid.uuid4()
    assert len(str(value).split("-")) == 5
