# mypy --check-untyped-defs tests/test_protocol.py

from typing import Protocol, Tuple

import pytest

pytestmark = pytest.mark.protocol


class Foo(Protocol):
    def to_int(self, value: str) -> int:
        ...

    def to_str(self, value: int) -> str:
        ...


class FooImpl:
    def to_int(self, value: str) -> int:
        return int(value)

    def to_str(self, value: int) -> str:
        return str(value)


def perform_to_int_str(f: Foo) -> Tuple[int, str]:
    return f.to_int("100"), f.to_str(100)


def test_performer():
    assert (100, "100") == perform_to_int_str(FooImpl())
