import functools


def test_partial():
    def foo(a: str, b: str, c: str):
        return f"{a}-{b}-{c}"

    new_foo = functools.partial(foo, "A", "B")

    assert "A-B-C" == new_foo("C")


def test_lru_cache():
    @functools.lru_cache
    def foo(x: int, y: int):
        return x + y

    assert 5 == foo(2, 3)
    assert 5 == foo(2, 3)
