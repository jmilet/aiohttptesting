from unittest.mock import AsyncMock, patch

import pytest

import pruebas.workers.main_consumer
from pruebas.settings import Settings
from pruebas.workers.main_consumer import main


@pytest.mark.asyncio
async def test_consumer_reads_from_queue():
    redis_db = AsyncMock()
    redis_db.brpop.return_value = "the value"

    assert await pruebas.workers.main_consumer.get_value_from_queue(redis_db=redis_db, queue="the queue") == "the value"


@pytest.mark.asyncio
@patch.object(pruebas.workers.main_consumer, "main_loop")
@patch.object(pruebas.workers.main_consumer.aioredis, "from_url", return_value="the redis db")
async def test_main_loop(from_url, main_loop):
    settings = Settings(redis_service_host="http://redishost.com", redis_service_port=2000, redis_queue="the queue")
    await main(settings)

    main_loop.assert_awaited_once_with(redis_db="the redis db", queue="the queue")

    from_url.assert_called_with("redis://http://redishost.com:2000")
