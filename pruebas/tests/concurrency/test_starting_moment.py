import asyncio
from datetime import datetime

import pytest


def log(*k, new_line=False):
    if new_line:
        print("\n")
    print(datetime.now(), *k)


async def foo(t):
    log("ini", t)
    await asyncio.sleep(t)
    log("end", t)


@pytest.mark.skip(reason="It's a slow test just for -s mode in CLI")
@pytest.mark.asyncio
async def test_starting_moment():
    num_tasks = 5

    log("1. task as_completed -------------", new_line=True)

    tasks = [asyncio.create_task(foo(i)) for i in range(num_tasks)]
    await asyncio.sleep(5)
    log("***")
    for t in asyncio.as_completed(tasks):
        await t

    log("2. task gather -------------------------------------------------------")

    tasks = [asyncio.create_task(foo(i)) for i in range(num_tasks)]
    await asyncio.sleep(5)
    log("***")
    await asyncio.gather(*tasks)

    log("3. co-routine as_completed -------------------------------------------")

    tasks = [foo(i) for i in range(num_tasks)]
    await asyncio.sleep(5)
    log("***")
    for t in asyncio.as_completed(tasks):
        await t

    log("4. co-routine gather ------------------------------------------------")

    tasks = [foo(i) for i in range(num_tasks)]
    await asyncio.sleep(5)
    log("***")
    await asyncio.gather(*tasks)
