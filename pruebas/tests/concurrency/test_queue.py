import asyncio
from asyncio import PriorityQueue, Queue, Task
from contextlib import asynccontextmanager
from typing import Any, Callable, Coroutine, List, Tuple

import pytest


class Pool:
    def __init__(self):
        self._tasks: List[Task] = []

    def add(self, n: int, action: Callable[[], Coroutine]) -> None:
        for _ in range(n):
            self._tasks.append(asyncio.create_task(action()))

    async def close(self) -> Any:
        for task in self._tasks:
            task.cancel()
        return await asyncio.gather(*self._tasks, return_exceptions=True)


@asynccontextmanager
async def new_pool():
    try:
        pool = Pool()
        yield pool
    finally:
        await pool.close()


async def consumer_1(input, output):
    while True:
        await output.put(f"Processed: {await input.get()}")
        input.task_done()


async def consumer_2(input):
    while True:
        print(await input.get())
        input.task_done()


@pytest.mark.asyncio
async def test_consumption(capsys):
    work: Queue[int] = Queue(maxsize=1)
    result: Queue[str] = Queue(maxsize=1)

    async with new_pool() as pool:
        pool.add(5, lambda: consumer_1(work, result))
        pool.add(5, lambda: consumer_2(result))

        for i in range(10):
            await work.put(i)

        await work.join()

    out, _err = capsys.readouterr()

    expected = [
        "Processed: 0",
        "Processed: 1",
        "Processed: 2",
        "Processed: 3",
        "Processed: 4",
        "Processed: 5",
        "Processed: 6",
        "Processed: 7",
        "Processed: 8",
        "Processed: 9",
        "",
    ]

    assert sorted(expected) == sorted(out.split("\n"))


@pytest.mark.asyncio
async def test_priority_queue():
    q: PriorityQueue[Tuple[int, str]] = PriorityQueue()

    await q.put((1, "second"))
    await q.put((0, "first"))

    assert [(0, "first"), (1, "second")] == [await q.get() for i in range(2)]
