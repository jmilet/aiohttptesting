import asyncio
import time

import pytest


@pytest.mark.asyncio
async def test_as_completed():
    tasks = [asyncio.sleep(i / 10) for i in range(10)]  # type:ignore

    results = []
    for task in asyncio.as_completed(tasks):
        await task
        results.append(time.time())

    # Each array element must be lower than the next one.
    for i in range(1, len(results)):
        assert results[i - 1] < results[i]
