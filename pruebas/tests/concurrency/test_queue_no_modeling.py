import asyncio
from asyncio import FIRST_COMPLETED, Queue
from random import randint
from typing import Any, Iterable

import pytest

NUM_ELEMENTS_TO_PRODUCE = 10
NUM_WORKERS = NUM_ELEMENTS_TO_PRODUCE // 2


async def consumer_job(input: Queue, output: Queue):
    while True:
        n = await input.get()
        await output.put(f"ping {n}...")
        await asyncio.sleep(1)
        input.task_done()


async def collector_job(input: Queue, output: list[str]):
    while True:
        output.append(await input.get())
        input.task_done()


@pytest.mark.asyncio
async def test_proudcer_consumer():
    result: list[str] = []

    # Create queue.
    input = Queue[int](maxsize=1)
    output = Queue[int](maxsize=1)

    # Create workers.
    consumers = [asyncio.create_task(consumer_job(input, output)) for n in range(NUM_WORKERS)]
    collector = asyncio.create_task(collector_job(output, result))

    # Produce some work.
    [await input.put(n) for n in range(NUM_ELEMENTS_TO_PRODUCE)]

    # Wait for the consumers to finish.
    await asyncio.wait_for(input.join(), timeout=10)

    # Wait for the collector to finish.
    await asyncio.wait_for(output.join(), timeout=10)

    assert result == [
        "ping 0...",
        "ping 1...",
        "ping 2...",
        "ping 3...",
        "ping 4...",
        "ping 5...",
        "ping 6...",
        "ping 7...",
        "ping 8...",
        "ping 9...",
    ]

    # Cancel tasks.
    [t.cancel() for t in consumers]
    collector.cancel()


async def parallel(work: Iterable[Any], task, size: int = 2):
    """Process work keeping {size} constant tasks"""

    pending = set[Any]()

    # Consume the work.
    for w in work:
        if len(pending) >= size:
            done, pending = await asyncio.wait(pending, return_when=FIRST_COMPLETED)
            for d in done:
                yield await d

        pending.add(asyncio.create_task(task(w)))

    # Wait for the work's tail to finish.
    for d in asyncio.as_completed(pending):
        yield await d


async def thing_to_do(n: int) -> int | Exception:
    try:
        if n == 5:
            1 / 0

        print(f"--- {n} started -----------------------")

        async for r in parallel(range(6), thing_to_do_slow, size=3):
            print(f"{n} -> slow finished {r}")

        print(f"--- {n} finished -----------------------")

        return n
    except Exception as ex:
        return ex


async def thing_to_do_slow(n: int) -> None | Exception:
    await asyncio.sleep(randint(5, 10))
    return None


@pytest.mark.asyncio
async def test_producer_loop():
    # Work is a generetor.
    work = range(10)

    # Trasform the output generator into a list.
    result = [r async for r in parallel(work, thing_to_do, size=3)]

    # Separate exceptions from values.
    exceptions = [r for r in result if isinstance(r, Exception)]
    values = [r for r in result if not isinstance(r, Exception)]

    # Assert we got the number's 5 exception.
    assert len(exceptions) == 1 and isinstance(exceptions[0], ZeroDivisionError)

    # Assert we got the rest of the values.
    assert sorted(values) == [0, 1, 2, 3, 4, 6, 7, 8, 9]
