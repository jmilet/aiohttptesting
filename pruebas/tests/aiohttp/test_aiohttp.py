import os
from unittest.mock import MagicMock

import pydantic
import pytest
from aiohttp import ClientSession
from pydantic import AnyUrl, parse_obj_as

pytestmark = pytest.mark.asyncio


@pytest.fixture
async def session():
    async with ClientSession() as session:
        yield session


@pytest.fixture
async def token():
    token = os.environ.get("GITHUB_TOKEN")

    if not token:
        pytest.skip("No github token defined")

    return token


async def _verb(session, *, verb, model=None, **kw):
    async with verb(**kw) as resp:
        if model:
            model = parse_obj_as(model, await resp.json())

        return resp.status, model


async def post(session, *, model=None, **kw):
    return await _verb(session, verb=session.post, model=model, **kw)


async def get(session, *, model=None, **kw):
    return await _verb(session, verb=session.get, model=model, **kw)


@pytest.mark.skip("we are banned")
async def test_post(session):
    class Response(pydantic.BaseModel):
        success: bool

    data = {"id": 78912, "customer": "Jason Sweet", "quantity": 1, "price": 18.00}

    status, resp = await post(session, model=Response, url="https://reqbin.com/echo/post/json", json=data)

    assert status == 200
    assert resp == {"success": True}


@pytest.mark.skip("we are banned")
async def test_post_no_model(session):
    data = {"id": 78912, "customer": "Jason Sweet", "quantity": 1, "price": 18.00}

    resp = await post(session, url="https://reqbin.com/echo/post/json", json=data)

    assert resp == (200, None)


async def test_get(session):
    class Response(pydantic.BaseModel):
        text: str

    status, resp = await get(session, model=Response, url="http://echo.jsontest.com/text/hola")

    assert status == 200
    assert resp == {"text": "hola"}


async def test_get_gists(token, session):
    class File(pydantic.BaseModel):
        filename: str
        raw_url: AnyUrl

    class Gist(pydantic.BaseModel):
        id: str
        files: dict[str, File]

    params = {"per_page": 10, "page": -1}
    headers = {"Authorization": f"token {token}"}

    while True:
        params["page"] += 1
        status, gists = await get(session, model=list[Gist], url="https://api.github.com/gists", params=params, headers=headers)
        assert status == 200
        if not gists:
            break


async def test_parsing_error():
    incorrect_data = {"pepe": "hola"}

    session = MagicMock()
    session.get.return_value.__aenter__.return_value.status = 400
    session.get.return_value.__aenter__.return_value.json.return_value = incorrect_data

    with pytest.raises(pydantic.error_wrappers.ValidationError):
        await get(session, model=list, url="https://api.github.com/gists")
