import io
from contextlib import contextmanager, redirect_stdout, suppress
from unittest.mock import AsyncMock, MagicMock, Mock, call

import pytest


@contextmanager
def catch():
    print("---enter---")
    try:
        yield
    except Exception as ex:
        print("---caught---", ex)
    finally:
        print("---exit---")


@contextmanager
def log_before_after():
    print("---enter---")
    try:
        yield
    finally:
        print("---exit---")


@contextmanager
def defer(*actions):
    try:
        yield
    finally:
        for action in actions:
            action()


def test_with_context_manager(capsys):  # Capture stdout via fixture
    with log_before_after():
        print("test")

    out, _err = capsys.readouterr()

    assert out == "---enter---\ntest\n---exit---\n"


def test_redirect_std_output():  # Capture stdout via context manager
    with redirect_stdout(io.StringIO()) as output, log_before_after():
        print("test")

    output.seek(0)

    assert output.read().split("\n") == ["---enter---", "test", "---exit---", ""]


def test_suppress():
    with suppress(ZeroDivisionError):
        1 / 0


def test_defer():
    def clean_up_1():
        print("clean_up_1")

    def clean_up_2():
        print("clean_up_2")

    with redirect_stdout(io.StringIO()) as output, defer(clean_up_1, clean_up_2):
        print("mola")

    output.seek(0)

    assert output.read().split("\n") == ["mola", "clean_up_1", "clean_up_2", ""]


def test_exception_in_context_manager():
    with redirect_stdout(io.StringIO()) as output:
        with catch():
            1 / 0

    output.seek(0)

    assert output.read().split("\n") == ["---enter---", "---caught--- division by zero", "---exit---", ""]


def test_context_manager_with_regular_mock():
    """
    For some reason, in the mock class, all non existing attributes are returned as mocks,
    except for the magic methods that are getattr'ed.

    That's the reason we have to configure them manually as mocks. The magic mock takes care
    of all these.

    https://docs.python.org/3/library/unittest.mock.html#mocking-magic-methods
    """

    # Manual magic method configuration.
    the_mock = Mock()
    the_mock.__enter__ = Mock()
    the_mock.__enter__.return_value = "the response"
    the_mock.__exit__ = Mock()

    with the_mock as r:
        assert r == "the response"

    # The same, but giving the return value in the constructor.
    the_mock = Mock()
    the_mock.__enter__ = Mock(return_value="the response")
    the_mock.__exit__ = Mock()

    with the_mock as r:
        assert r == "the response"


def test_context_manager_with_regular_magic_mock():
    # The magic mock already configures the magic methods.
    the_mock = MagicMock()
    the_mock.__enter__.return_value = "the response"

    with the_mock as r:
        assert r == "the response"


@pytest.mark.asyncio
async def test_mock_calls():
    the_mock = AsyncMock()

    with the_mock as value:
        value.foo("hola mundo")

    assert the_mock.mock_calls == [
        call.__enter__(),
        call.__enter__().foo("hola mundo"),
        call.__exit__(None, None, None),
    ]
