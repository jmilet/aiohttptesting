import aiofiles  # type: ignore
import pytest

# https://github.com/Tinche/aiofiles
# https://www.twilio.com/blog/working-with-files-asynchronously-in-python-using-aiofiles-and-asyncio


@pytest.mark.asyncio
async def test_aiofile():
    async with aiofiles.open("/etc/passwd", "rt") as f:
        pass_file = await f.read()

    assert pass_file != ""
