import pytest
from httpx import AsyncClient

from pruebas.main import app
from pruebas.model.foo import Foo
from pruebas.router.foo import get_value

pytestmark = pytest.mark.router


@pytest.fixture()
@pytest.mark.asyncio
async def client():
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client


@pytest.mark.asyncio
async def test_foo(client):
    response = await client.get("/foo")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}


@pytest.mark.asyncio
async def test_process_time_is_in_header(client):
    response = await client.get("/foo")
    assert response.status_code == 200
    assert "X-Process-Time" in response.headers
    assert len(response.headers["X-Process-Time"]) > 0


@pytest.mark.asyncio
async def test_metrics(client):
    response = await client.get("/metrics")
    assert response.status_code == 200
    assert response.text.find("starlette") >= 0


@pytest.mark.asyncio
async def test_put_foo_setting_json(client):
    foo = Foo(the_id=100, the_description="the pepe")
    response = await client.put("/foo", json=foo.__dict__)
    assert response.status_code == 200
    assert response.json() == foo
    print(foo)
    print(foo.__dict__)
    print(response.json())


@pytest.mark.asyncio
async def test_get_dependency(client):
    response = await client.get("/foo/dependency")
    assert response.status_code == 200
    value1 = response.json()

    response = await client.get("/foo/dependency")
    assert response.status_code == 200
    value2 = response.json()

    assert value1 == value2


@pytest.mark.asyncio
async def test_get_dependency_params(client):
    response = await client.get("/foo/dependency/params", params={"q": "hola", "z": "que tal"})
    assert response.status_code == 200
    value1 = response.json()

    response = await client.get("/foo/dependency/params", params={"q": "hola", "z": "que tal"})
    assert response.status_code == 200
    value2 = response.json()

    assert value1 == value2
    assert "hola-que tal" in value1


@pytest.mark.asyncio
async def test_get_dependency_params_with_mock(client):
    def _get_value():
        return 100

    app.dependency_overrides[get_value] = _get_value

    try:
        response = await client.get("/foo/dependency/params", params={"q": "hola", "z": "que tal"})
        assert response.status_code == 200
        value1 = response.json()
        assert value1 == "100-hola-que tal"

    finally:
        app.dependency_overrides = {}


@pytest.mark.asyncio
async def test_health(client):
    response = await client.get("/foo/health")
    assert response.status_code == 200
    assert response.text == ""
