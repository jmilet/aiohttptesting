import json

import pytest
from fastapi.testclient import TestClient

from pruebas.main import app
from pruebas.model.foo import Foo

pytestmark = pytest.mark.router


@pytest.fixture(scope="module")
def client():
    return TestClient(app)


def test_foo(client):
    response = client.get("/foo")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}


def test_process_time_is_in_header(client):
    response = client.get("/foo")
    assert response.status_code == 200
    assert "X-Process-Time" in response.headers
    assert len(response.headers["X-Process-Time"]) > 0


def test_metrics(client):
    response = client.get("/metrics")
    assert response.status_code == 200
    assert response.text.find("starlette") >= 0


def test_put_foo_setting_json(client):
    foo = Foo(the_id=100, the_description="the pepe")
    response = client.put("/foo", json=foo.__dict__)
    assert response.status_code == 200
    assert response.json() == foo
    print(foo)
    print(foo.__dict__)
    print(response.json())


def test_put_foo_setting_data(client):
    foo = dict(the_id=100, the_description="the pepe")
    response = client.put("/foo", data=json.dumps(foo))
    assert response.status_code == 200
    assert response.json() == foo
    print(foo)
    print(response.json())
