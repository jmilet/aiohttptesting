from unittest.mock import AsyncMock, patch

import pytest
from httpx import AsyncClient

from pruebas.main import app

pytestmark = pytest.mark.router


@pytest.fixture()
@pytest.mark.asyncio
async def client():
    async with AsyncClient(app=app, base_url="http://test") as client:
        yield client


@pytest.mark.asyncio
@patch("pruebas.router.redis.redis_database")
async def test_foo(redis_database, client):
    redis_database.return_value = AsyncMock()

    headers = {"content-type": "text/plain"}

    response = await client.post("/redis/publish", data="the data", headers=headers)
    assert response.status_code == 200
    assert response.text == ""
