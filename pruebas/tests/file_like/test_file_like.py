import io


def test_file_like_string():
    data = "this is the data"

    the_file = io.StringIO()
    the_file.write(data)
    the_file.seek(0)

    assert the_file.readlines() == [data]
