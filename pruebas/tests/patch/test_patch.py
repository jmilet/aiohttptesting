from unittest.mock import MagicMock, patch

import pytest  # type: ignore

pytestmark = pytest.mark.patch


@pytest.fixture
def abc():
    print("1. --------")
    yield "valor"
    print("2. --------")
    return 10, 20


def test_foo(abc):
    print("**************")
    assert abc == "valor"


class Foo:
    def foa(self):
        return "soy foa"

    def foo(self):
        return "original"


def test_patch_class():
    with patch("tests.patch.test_patch.Foo") as klass:
        klass.return_value.foo.return_value = "el mockeado"
        assert "el mockeado" == Foo().foo()
        assert isinstance(Foo().foa(), MagicMock)

    assert "original" == Foo().foo()
    assert "soy foa" == Foo().foa()


def test_patch_method():
    with patch.object(Foo, "foo") as method:
        method.return_value = "el mockeado"
        assert "el mockeado" == Foo().foo()
        assert "soy foa" == Foo().foa()

    assert "original" == Foo().foo()
    assert "soy foa" == Foo().foa()


@patch("tests.patch.test_patch.int")
def test_patch_builtin_int(mock_int):
    mock_int.return_value = "hola que tal"
    assert "hola que tal" == int(5)


@patch("tests.patch.test_patch.len")
def test_patch_builtin_len(mock_len):
    mock_len.return_value = 10000
    assert len("hola que tal") == 10000
