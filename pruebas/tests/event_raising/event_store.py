import json
import time
from typing import Any, Callable, List, Optional

import pydantic


class Event(pydantic.BaseModel):
    name: str
    data: str
    order: int
    timestamp: int


class EventStore:
    def __init__(self, now: Callable[[], int] = None):
        self._events: List[Event] = []
        self._now = now or time.time

    def publish(self, name: str, data: Any):
        self._events.append(Event(name=name, data=json.dumps(data), order=len(self._events) + 1, timestamp=int(self._now())))

    def get_event(self, n: int) -> Optional[Event]:
        if n > len(self._events):
            return None
        return self._events[n]
