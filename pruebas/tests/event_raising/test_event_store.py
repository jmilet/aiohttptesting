import json
from unittest.mock import Mock

from tests.event_raising.event_store import Event, EventStore  # type:ignore


def test_event_store_with_lambda_timestamp():
    ev = EventStore(now=lambda: 100)

    ev.publish("uno", {"data": "the uno data"})
    ev.publish("dos", {"data": "the dos data"})

    assert ev.get_event(0) == Event(name="uno", data=json.dumps({"data": "the uno data"}), order=1, timestamp=100)
    assert ev.get_event(1) == Event(name="dos", data=json.dumps({"data": "the dos data"}), order=2, timestamp=100)
    assert ev.get_event(3) is None


def test_event_store_with_mock_timestamp():
    ev = EventStore(now=Mock(side_effect=[100, 200]))

    ev.publish("uno", {"data": "the uno data"})
    ev.publish("dos", {"data": "the dos data"})

    assert ev.get_event(0) == Event(name="uno", data=json.dumps({"data": "the uno data"}), order=1, timestamp=100)
    assert ev.get_event(1) == Event(name="dos", data=json.dumps({"data": "the dos data"}), order=2, timestamp=200)
    assert ev.get_event(3) is None


def test_event_store_natural_timestamp():
    ev = EventStore()

    ev.publish("uno", {"data": "the uno data"})
    ev.publish("dos", {"data": "the dos data"})

    assert ev.get_event(0).timestamp > 0
    assert ev.get_event(1).timestamp > 0
