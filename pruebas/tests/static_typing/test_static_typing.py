from typing import Optional


def test_optional():
    def foo(value: str):
        return value

    def say_hi(name: Optional[str] = None):
        if name is not None:
            name_foo: str = name

        return "Hi " + foo(name_foo)

    assert say_hi("juan") == "Hi juan"
