import os
from typing import Generic, Optional, TypeVar
from unittest.mock import patch
from uuid import uuid4

import pydantic
import pytest
from pydantic import BaseModel, BaseSettings, Field, HttpUrl, ValidationError, error_wrappers, parse_obj_as
from pydantic.generics import GenericModel
from typing_extensions import Annotated


class MySettings(BaseSettings):
    value_one: str


def test_base_settings_from_environment():
    with pytest.raises(error_wrappers.ValidationError):
        MySettings()

    with patch.dict("os.environ", {"VALUE_ONE": "pepe"}):
        assert os.environ["VALUE_ONE"] == "pepe"


def test_base_settings_from_object():
    the_obj = {"value_one": "pepe"}
    assert "pepe" == MySettings.parse_obj(the_obj).value_one


def test_base_settings_from_raw():
    the_raw = '{"value_one": "pepe"}'
    assert "pepe" == MySettings.parse_raw(the_raw).value_one


def test_field_mapping():
    T = TypeVar("T")

    class Foo(GenericModel, Generic[T]):
        b: Optional[T]
        c: Optional[T]

        def __init__(self, **kwargs):
            kwargs["b"] = kwargs["a"]["b"]
            super().__init__(**kwargs)

    record = Foo[str].parse_obj(
        {
            "a": {
                "b": "hola",
            },
            "c": "que tal",
            "d": "no entra",
        },
    )
    assert record.b == "hola"
    assert record.c == "que tal"


def test_url():
    class Model(BaseModel):
        url: HttpUrl

    with pytest.raises(ValidationError):
        Model(url="http://localhost")

    m = Model(url="http://localhost.com/abc")
    assert m.url.host == "localhost.com"

    m.url = parse_obj_as(HttpUrl, "http://localhost.com/other")
    assert m.url.path == "/other"


def test_annotated_field():
    class Foo(BaseModel):
        id: Annotated[str, Field(default_factory=lambda: uuid4().hex)]
        name: Annotated[str, Field(max_length=10)] = "Bar"

    Foo(name="foo")

    with pytest.raises(pydantic.error_wrappers.ValidationError) as ex:
        Foo(name="foo" * 10)

    assert ex.value.errors() == [
        {"loc": ("name",), "msg": "ensure this value has at most 10 characters", "type": "value_error.any_str.max_length", "ctx": {"limit_value": 10}}
    ]


def test_field_funcion():
    # WARNING: Validations are only triggered on object creation time, not on set time.

    class Foo(BaseModel):
        name: str = Field(..., regex=r"^H.{2}A")

    Foo(name="HOLA")

    with pytest.raises(pydantic.error_wrappers.ValidationError) as ex:
        Foo(name="HOLI")

    assert ex.value.errors() == [
        {"ctx": {"pattern": "^H.{2}A"}, "loc": ("name",), "msg": 'string does not match regex "^H.{2}A"', "type": "value_error.str.regex"}
    ]


def test_allow_mutation_false():
    class Foo(BaseModel):
        a: str = "hola"
        b: dict = {}

        class Config:
            allow_mutation = False

    with pytest.raises(TypeError) as ex:
        Foo().a = "hola"

    assert str(ex.value) == '"Foo" is immutable and does not support item assignment'
