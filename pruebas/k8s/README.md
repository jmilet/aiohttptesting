## Docker environment for k8s

The container has to be created in the minikube context.

```
eval $(minikube docker-env)
make -B docker
```

## Start
```
minikube start

minikube start --nodes 2

# alias kubectl="minikube kubectl --"
alias k=kubectl

minikube dashboard

k apply -f k8s.yml
```

## Use
```
k get deployments
k get namespaces
k get pods
k get events --sort-by='.lastTimestamp'
k logs <pod-name>
k describe pod <pod-name>

# Get a shell.
k exec --stdin --tty <pod-name> -- /bin/sh
k exec <pod-name> -- ps aux

k config set-context --current --namespace=pruebas-namespace

k delete deployment pruebas-deployment

k apply -f k8s.yml -n pruebas-namespace

k rollout restart deployment pruebas-deployment -n pruebas-namespace

k exec pruebas-deployment-7d7c87644-nx5xp -- printenv

k exec --stdin --tty $(k get pods --no-headers -o custom-columns=":metadata.name"|grep pruebas-deployment|head -1)  -- /bin/sh

k exec $(k get pods --no-headers -o custom-columns=":metadata.name"|grep pruebas-deployment|head -1) -- printenv

k scale deployment/pruebas-deployment --replicas=10

watch -n 1 "kubectl get pods"

k delete jobs --all

k delete cronjobs.batch hello

k config set-context pruebas --namespace=pruebas

k config use-context pruebas
```



## Shutdown
```
minikube stop
minikube delete
```